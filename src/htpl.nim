import ast_pattern_matching, macros, options, strformat, strutils


type 
  HtmlNode = ref object
    ids: seq[string]
    tag: string
    classes: seq[string]
    text: string
    children: seq[HtmlNode]

proc `$`(node: HtmlNode): string =
  result = fmt"<{node.tag}"
  if node.classes.len > 0:
    result.add " class=\"" & node.classes.join(" ") & "\""
  if node.ids.len > 0:
    result.add " id=\"" & node.ids.join(" ") & "\""
  result.add(">")
  result.add(node.text)

  for child in node.children:
    result.add($child)
  result.add(fmt"</{node.tag}>")


func newHtmlNode(): HtmlNode
proc parseHtmlNodeChildren(node: NimNode): seq[HtmlNode] 
proc parseHtmlNodeBody(node: NimNode): seq[HtmlNode]

proc processHtmlParts(tag: NimNode, attributes, children: Option[NimNode]): tuple[
    name: string,
    classes: Option[seq[string]],
    ids: Option[seq[string]],
    text: Option[string]
  ] =
  assert tag.kind == nnkIdent
  var 
    text: Option[string] = none(string)
    ids: Option[seq[string]] = none(seq[string])
    classes: Option[seq[string]] = none(seq[string])

  if isSome(attributes):
    let attrs = attributes.get
    classes = some(newSeq[string]())
    assert attrs.kind == nnkExprEqExpr
    for idx, attr in attrs.pairs:
      attr.matchAstRecursive:
      of ident"className":
        let pref = attrs[idx + 1]
        assert pref.kind == nnkPrefix
        for className in pref.findChild(it.kind == nnkBracket).children:
          assert className.kind == nnkStrLit
          classes.get.add(className.strVal)
      of ident"id":
        let pref = attrs[idx + 1]
        assert pref.kind == nnkPrefix
        for idName in pref.findChild(it.kind == nnkBracket).children:
          assert idName.kind == nnkStrLit
          ids.get.add(idName.strVal)

  if isSome(children):
    let ch = children.get
    assert ch.kind == nnkStmtList
    let txt = ch.findChild(it.kind == nnkCommand)
    if not txt.isNil:
      text = some(txt.findChild(it.kind == nnkStrLit).strVal)

  return (tag.strVal, classes, ids, text)


func newHtmlNode(): HtmlNode = 
  new result
  result.children = @[]
  result.classes = @[]
  result.ids = @[]


proc setHtmlAttrs(node: var HtmlNode, tag: string, classes, ids: Option[seq[string]], text: Option[string]) =
  node.tag = tag

  if isSome(classes):
    node.classes = classes.get

  if isSome(ids):
    node.ids = ids.get

  if isSome(text):
    node.text = text.get


proc newHtmlNode(tag: NimNode): HtmlNode =
  ## Creates a new Html Node
  result = newHtmlNode()

  let (tag, classes, ids, text) = processHtmlParts(tag, none(NimNode), none(NimNode))
  result.setHtmlAttrs(tag, classes, ids, text)


proc newHtmlNode(tag: NimNode, children: NimNode): HtmlNode =
  ## Creates a new Html Node
  result = newHtmlNode()

  let (tag, classes, ids, text) = processHtmlParts(tag, none(NimNode), some(children))
  result.setHtmlAttrs(tag, classes, ids, text)


proc newHtmlNode(tag: NimNode, attributes: NimNode): HtmlNode =
  ## Creates a new Html Node
  result = newHtmlNode()

  let (tag, classes, ids, text) = processHtmlParts(tag, some(attributes), none(NimNode))
  result.setHtmlAttrs(tag, classes, ids, text)


proc newHtmlNode(tag: NimNode, attributes, children: NimNode): HtmlNode =
  ## Creates a new Html Node
  result = newHtmlNode()

  let (tag, classes, ids, text) = processHtmlParts(tag, some(attributes), some(children))
  result.setHtmlAttrs(tag, classes, ids, text)


proc updateHtmlNode(htmlNode: var HtmlNode, tag, children: NimNode) =
  ## Update a Html Node with attributes and tags
  let (tag, classes, ids, text) = processHtmlParts(tag, none(NimNode), some(children))
  htmlNode.setHtmlAttrs(tag, classes, ids, text)


proc updateHtmlNode(htmlNode: var HtmlNode, tag, attributes, children: NimNode) =
  ## Update a Html Node with attributes and tags
  let (tag, classes, ids, text) = processHtmlParts(tag, some(attributes), some(children))
  htmlNode.setHtmlAttrs(tag, classes, ids, text)


proc parseHtmlNodeChildren(node: NimNode): seq[HtmlNode] =
  ## Parse Children elements of nodes
  assert node.kind == nnkStmtList
  var cSeq = newSeq[HtmlNode]()

  node.matchAstRecursive:
  of nnkCall(`tag` @ nnkIdent, `children` @ nnkStmtList):
    let childNode = newHtmlNode(tag, children = children)
    childNode.children = parseHtmlNodeChildren(children)
    cSeq.add(childNode)
  of nnkCommand:
    echo "Got a command"
  of nnkCall(`tag` @ nnkIdent, `attributes` @ nnkExprEqExpr, `children` @ nnkStmtList):
    let childNode = newHtmlNode(tag, attributes, children)
    childNode.children = parseHtmlNodeChildren(children)
    cSeq.add(childNode)

  return cSeq


proc parseHtmlNodeBody(node: NimNode): seq[HtmlNode] =
  ## Parse a Html Node, building children 
  assert node.kind == nnkStmtList
  var htmlNodes: seq[HtmlNode] = @[]

  node.matchAstRecursive:
  of `arg` @ nnkCall(`tag` @ nnkIdent, `children` @ nnkStmtList):
    # Match an element with only a tag name and children
    var htmlNode = newHtmlNode()
    updateHtmlNode(htmlNode, tag, children)
    htmlNode.children = parseHtmlNodeChildren(children)
    htmlNodes.add(htmlNode)
  of `arg` @ nnkCall(`tag` @ nnkIdent, `attributes` @ nnkExprEqExpr, `children` @ nnkStmtList):
    # Match an element with attributes, children and a tag name
    var htmlNode = newHtmlNode()
    updateHtmlNode(htmlNode, tag, attributes, children)
    htmlNode.children = parseHtmlNodeChildren(children)
    htmlNodes.add(htmlNode)
  of `arg` @ nnkCall(nnkCommand(ident"text", `text` @ nnkStrLit)):
    var htmlNode = newHtmlNode()
    htmlNode.text = text.strVal
    htmlNodes.add(htmlNode)

  return htmlNodes


macro parseHtml(): string = 
  let ast = quote do:
    html:
      body:
        span:
          text "aa"
        span(className = @["bbb", "ddda", "hhh"]):
          text "bb"
          span(c = 5):
            text "ju"
          span(c = 6, id = "ggg"):
            text "jl"
        span(c = 2):
          text "hh"
        span:
          text "gg"

  echo ast.treerepr

  var
    tree = newHtmlNode(ident"html")
  ast.matchAst:
  # This needs a 1 or more for args
  of `html` @ nnkCall(
      `tag` @ ident"html",
      `nodeBody`
    ):
    # Valid html block
    var body = newHtmlNode(ident"body")

    tree.children.add(body)
    body.children = nodeBody.parseHtmlNodeBody()
  return newStrLitNode("test")
  

when isMainModule:
  let htmltree = parseHtml()
  assert htmltree == "test"
